# Dst #
Daylight Savings Time (DST) class.

Also includes methods for time and date calculations.

Initially designed for Arduino platforms, although can be used for other
embedded systems since it has no Arduino dependencies.

The only system dependency is <time.h> for the definition of struct tm - the
standard structure containing a calendar date and time broken down into its 
components.  The class does not expect any time functions to be provided
by the system.  It is completely self-contained.

The Dst class provides static methods for determining leap year, day of the 
week, day of the year, and enhanced gmtime() and mktime() methods that convert 
times referenced from either the Unix epoch (1970) or the NTP epoch (1900).

A Dst object is configured with a structure that describes the Daylight Savings
Time rules for the local region of interest.  This allows the Dst class to
work in any region.  Once configured, the user can call Dst::isDst() with
a time structure to determine if Daylight Savings Time is in effect.

An example unit test sketch is provided.  It has a dependency on the aunit
Arduino unit test library.

### Documentation ###
Documentation is contained in the Dst.h header file.

### Author ###
Tom Kerr AB3GY

### License ###
Released under the MIT License
https://opensource.org/licenses/MIT
