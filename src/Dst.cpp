/******************************************************************************
 * Dst.cpp
 * Copyright (c) 2017 Thomas Kerr AB3GY
 *
 * Released under the MIT License (MIT). 
 * See http://opensource.org/licenses/MIT
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 ******************************************************************************/

/**
 * @file
 * @brief
 * A self-contained class to manage Daylight Savings Time.
 *
 * Also includes methods for time and date calculations.
 *
 * Initially designed for Arduino platforms, although it can be used for other
 * embedded systems since it has no Arduino dependencies.
 *
 * The only system dependency is <time.h> for the definition of struct tm - the
 * standard structure containing a calendar date and time broken down into its 
 * components.  The class does not expect any time functions to be provided
 * by the system.  It is completely self-contained.
 */
 
/******************************************************************************
 * System include files.
 ******************************************************************************/
#include <string.h>


/******************************************************************************
 * Local include files.
 ******************************************************************************/
#include "Dst.h"


/******************************************************************************
 * Forward references.
 ******************************************************************************/

/******************************************************************************
 * Local definitions.
 ******************************************************************************/
#define NTP2UNIX     (2208988800UL)  //!< Conversion from NTP epoch to Unix epoch
#define EPOCH1972      (63072000UL)  //!< Conversion from Unix epoch to 1972 
#define MIN_SECS             (60UL)  //!< Seconds per minute
#define HOUR_SECS          (3600UL)  //!< Seconds per hour
#define DAY_SECS   (24UL*HOUR_SECS)  //!< Seconds per day
#define YEAR_SECS  (365UL*DAY_SECS)  //!< Seconds per year
#define LYEAR_SECS (366UL*DAY_SECS)  //!< Seconds per leap year
#define YEAR4_SECS ((3UL*YEAR_SECS) + LYEAR_SECS)  //!< Seconds per 4-year block


/******************************************************************************
 * Global objects and data.
 ******************************************************************************/
 
/******************************************************************************
 * Local data.
 ******************************************************************************/
static const int LAST_DAY[13]    = {0, 31, 28, 31,  30,  31,  30,  31,  31,  30,  31,  30,  31};
static const int TOTAL_DAYS[13]  = {0, 31, 59, 90, 120, 151, 181, 212, 243, 273, 304, 334, 365};
static const int TOTAL_LDAYS[13] = {0, 31, 60, 91, 121, 152, 182, 213, 244, 274, 305, 335, 366};


/******************************************************************************
 * Public methods and functions.
 ******************************************************************************/
 
/**************************************
 * Dst::Dst()
 **************************************/ 
Dst::Dst() : m_config({0,0,0,0,0,0,0,0})
{
}
 
 
/**************************************
 * Dst::Dst()
 **************************************/ 
Dst::Dst(const dst_config_t &config)
{
    m_config = config;
}

 
/**************************************
 * Dst::setConfig()
 **************************************/ 
void Dst::setConfig(const dst_config_t &config)
{
    m_config = config;
}

 
/**************************************
 * Dst::isDst()
 **************************************/ 
int Dst::isDst(const dst_time_t& time)
{
    int dst = 0;
    
    switch (status(time))
    {
        case DST_ENDING:
        case DST_IN_EFFECT:
            dst = 1;
            break;
            
        default:
            break;
    }
    
    return dst;
}


/**************************************
 * Dst::dstOffset()
 **************************************/ 
int32_t Dst::dstOffset(const dst_time_t& time)
{
    int32_t ret = 0;
    if (isDst(time))
    {
        ret = m_config.offset_secs;
    }
    return ret;
}


/**************************************
 * Dst::status()
 **************************************/
dst_status_t Dst::status(const dst_time_t& time)
{
    dst_status_t status = DST_STANDARD_TIME;
    
    // Continue only if DST is enabled.
    if (m_config.enabled)
    {
        // Case 1. In the middle of DST.
        if ((time.month > m_config.start_month) && (time.month < m_config.end_month))
        {
            status = DST_IN_EFFECT;
        }
        
        // Case 2. In the DST start month.
        else if (time.month == m_config.start_month)
        {
            int start = Dst::nthSunday(time.year, time.month, m_config.start_sunday);
            if (time.day > start)
            {
                // Past the start of DST.
                status = DST_IN_EFFECT;
            }
            else if (time.day == start)
            {
                if (time.hour >= m_config.start_hour)
                {
                    // Past the starting hour of DST.
                    status = DST_IN_EFFECT;
                }
                else
                {
                    // DST starts today.
                    status = DST_BEGINNING;
                }
            }
        }
        
        // Case 3. In the DST end month.
        else if (time.month == m_config.end_month)
        {
            int end = Dst::nthSunday(time.year, time.month, m_config.end_sunday);
            if (time.day < end)
            {
                // Haven't reached the end of DST.
                status = DST_IN_EFFECT;
            }
            else if (time.day == end)
            {
                if (time.hour < m_config.end_hour)
                {
                    // DST ends today.
                    status = DST_ENDING;
                }
            }
        }
    }
    
    return status;
}


/**************************************
 * Dst::tm2dst()
 **************************************/ 
void Dst::tm2dst(const struct tm* tmptr, dst_time_t* dstptr)
{
    memset(dstptr, 0, sizeof(dst_time_t));
    dstptr->year  = tmptr->tm_year + 1900;  // Convert from years since 1900
    dstptr->month = tmptr->tm_mon + 1;      // (0-11) -> (1-12)
    dstptr->day   = tmptr->tm_mday;
    dstptr->hour  = tmptr->tm_hour;
    dstptr->min   = tmptr->tm_min;
    dstptr->sec   = tmptr->tm_sec;
}


/**************************************
 * Dst::dst2tm()
 **************************************/ 
void Dst::dst2tm(const dst_time_t* dstptr, struct tm* tmptr)
{
    memset(tmptr, 0, sizeof(struct tm));
    tmptr->tm_year  = dstptr->year - 1900; // Convert to years since 1900
    tmptr->tm_mon   = dstptr->month - 1;   // (1-12) -> (0-11)
    tmptr->tm_mday  = dstptr->day;
    tmptr->tm_hour  = dstptr->hour;
    tmptr->tm_min   = dstptr->min;
    tmptr->tm_sec   = dstptr->sec;
    tmptr->tm_wday  = Dst::getWeekday(*dstptr) - 1;
    tmptr->tm_yday  = Dst::getDayNumber(*dstptr);
    tmptr->tm_isdst = isDst(*dstptr);
}


/**************************************
 * Dst::getDayNumber()
 **************************************/
int Dst::getDayNumber(const dst_time_t& time)
{
    int day = 0;
    
    // Validate the month for array lookup.
    if ((time.month < 1) || (time.month > 12)) return 0;
    
    // Add whole months.
    const int* pTotalDays = (Dst::isLeapYear(time.year)) ? TOTAL_LDAYS : TOTAL_DAYS;
    day += pTotalDays[time.month-1];
    
    // Add day of the month.
    day += time.day;
    
    return day;
}


/**************************************
 * Dst::getWeekday()
 **************************************/
int Dst::getWeekday(const dst_time_t& time)
{
    // Assumes Gregorian calendar and is limited to the years 1700 - 2499.
    // No range checking is performed, so beware of arrays out of bounds.
    
    // Compute the day of the week for the given date using 
    // the Key Value Method.
    // See, for example, http://mathforum.org/dr.math/faq/faq.calendar.html 
    // 1 = Sunday, 2 = Monday, ... 7 = Saturday
    
    static const int MONTH_TABLE[12] = {1, 4, 4, 0, 2, 5, 0, 3, 6, 1, 4, 6};
    static const int CENTURY_CODE[8] = {4, 2, 0, 6, 4, 2, 0, 6}; // 17nn - 24nn
    
    int y2 = time.year % 100 ;    // 2-digit year
    
    // Take the 2-digit year, divide by 4, and ignore the remainder.
    // Add the day of the month.
    // Add the month's key value from the table.
    int temp = (y2 / 4) + time.day + MONTH_TABLE[time.month-1];
    
    // If date is in January or February of a leap year, subtract 1.
    if (Dst::isLeapYear(time.year))
    {
        if ((time.month == 1) || (time.month == 2))
        {
            temp--;
        }
    }
    
    // Add century code as follows:
    // 1700s = 4; 1800s = 2; 1900s = 0; 2000s = 6, and the cycle repeats.
    int cc = CENTURY_CODE[(time.year / 100) - 17];
    temp += cc;
    
    // Add the last two digits of the year, divide by 7 and take the remainder.
    // 1 = Sunday ... 7 = Saturday
    int wkdy = (temp + y2) % 7;
    if (wkdy == 0) wkdy = 7;
    
    return wkdy;
}


/**************************************
 * Dst::gmtime()
 **************************************/
void Dst::gmtime(uint32_t timer, struct tm* tmptr, dst_epoch_t epoch)
{
    if (tmptr == NULL) return;
    const int* pTotalDays;
    
    // Convert to Unix epoch.
    if (epoch == DST_EPOCH_NTP)
    {
        timer -= NTP2UNIX;
    }
    
    // Convert to seconds since year 1972.
    // Year 1972 is a leap year, which makes the math a little easier.
    // Years 1900 and 1970 are not leap years.
    uint32_t secs = timer - EPOCH1972;
    
    // Calculate the year.
    uint32_t n4blks = secs / YEAR4_SECS;  // Number of 4-year blocks since 1972
    secs -= (n4blks * YEAR4_SECS);        // Seconds since the last 4-year block boundary
    uint32_t nyears = 0;                  // Years since the last 4-year block boundary
    if (secs >= LYEAR_SECS)               // We start on a leap year boundary
    {
        nyears++;
        secs -= LYEAR_SECS;
        while (secs >= YEAR_SECS)         // Might have up to two more non-leap years
        {
            nyears++;
            secs -= YEAR_SECS;
        }
    }
    int year = 1972 + (int)((4 * n4blks) + nyears);
    tmptr->tm_year = year - 1900;         // struct tm expects years since 1900
    
    // Calculate the day of the year.
    uint32_t yday = secs / DAY_SECS;
    tmptr->tm_yday = (int)yday;           // struct tm expects days since January 1
    
    // Calculate the month.
    pTotalDays = (Dst::isLeapYear(year)) ? TOTAL_LDAYS : TOTAL_DAYS;
    int m = 1;
    for (; m <= 12; m++)
    {
        if (yday < pTotalDays[m]) break;
    }
    --m;                                  // struct tm expects month in range 0 - 11
    tmptr->tm_mon = m;
    
    // Calculate the day of the month.
    secs -= (pTotalDays[m] * DAY_SECS);   // Seconds since the beginning of the month
    uint32_t day = secs / DAY_SECS;
    tmptr->tm_mday = (int)(day + 1);
    
    // Calculate the hour.
    secs -= (day * DAY_SECS);             // Seconds since the start of the day
    uint32_t hour = secs / HOUR_SECS;
    tmptr->tm_hour = (int)(hour);
    
    // Calculate the minutes.
    secs -= (hour * HOUR_SECS);           // Seconds since the start of the hour
    uint32_t min = secs / MIN_SECS;
    tmptr->tm_min = (int)(min);
    
    // Calculate the seconds.
    secs -= (min * MIN_SECS);
    tmptr->tm_sec = (int)(secs);
    
    // Calculate the weekday.
    dst_time_t tmptime;
    Dst::tm2dst(tmptr, &tmptime);
    tmptr->tm_wday = Dst::getWeekday(tmptime) - 1;
    
    // GMT has no DST.
    tmptr->tm_isdst = 0;
}


/**************************************
 * Dst::gmtime()
 **************************************/
void Dst::gmtime(uint32_t timer, dst_time_t* dstptr, dst_epoch_t epoch)
{
    struct tm tmptime;
    
    if (dstptr == NULL) return;
    Dst::gmtime(timer, &tmptime, epoch);
    Dst::tm2dst(&tmptime, dstptr);
}


/**************************************
 * Dst::mktime()
 **************************************/
uint32_t Dst::mktime(const dst_time_t* dstptr, dst_epoch_t epoch)
{
    // Range checks.
    if (dstptr == NULL) return 0;
    if ((dstptr->year < 1972) || (dstptr->year > 2037)) return 0;
    if ((dstptr->month < 1)   || (dstptr->month > 12))  return 0;
    if ((dstptr->day < 1)     || (dstptr->day > 31))    return 0;
    if ((dstptr->hour < 0)    || (dstptr->hour > 23))   return 0;
    if ((dstptr->min < 0)     || (dstptr->min > 59))    return 0;
    if ((dstptr->sec < 0)     || (dstptr->sec > 59))    return 0;

    // Convert to a 1972 epoch.
    // Year 1972 is a leap year, which makes the math a little easier.
    // Years 1900 and 1970 are not leap years.
    uint32_t time = EPOCH1972;
    
    // Add the NTP delta if selected.
    if (epoch == DST_EPOCH_NTP) time += NTP2UNIX;
    
    // Add the whole number years since 1972.
    uint32_t nyears = dstptr->year - 1972;  // Number of whole years since 1972
    uint32_t n4blks = nyears / 4UL;         // Number of 4-year blocks since 1972
    nyears -= (n4blks * 4UL);               // Number of remaining years (0 - 3)
    time += (n4blks * YEAR4_SECS);
    if (nyears > 0)
    {
        time += LYEAR_SECS;                 // First remaining year is a leap year
        --nyears;
        while (nyears > 0)
        {
            time += YEAR_SECS;              // Second and third remaining years are non-leap years
            --nyears;
        }
    }
    
    // Add the whole number of months.
    const int* pTotalDays = (Dst::isLeapYear(dstptr->year)) ? TOTAL_LDAYS : TOTAL_DAYS;
    time += (pTotalDays[dstptr->month-1] * DAY_SECS);
    
    // Add day of the month.
    time += ((dstptr->day - 1) * DAY_SECS);
    
    // Add hour, minute, and second.
    time += (dstptr->hour * HOUR_SECS);
    time += (dstptr->min  * MIN_SECS);
    time += dstptr->sec;
    
    return time;
}


/**************************************
 * Dst::isLeapYear()
 **************************************/
int Dst::isLeapYear(int year)
{
    int leap = 0;
    
    // NOTE: year is expected to be a 4-digit year.
    if ((year % 400) == 0)
    {
        // If year is divisible by 400, it is a leap year.
        leap = 1;
    }
    else if ((year % 100) == 0)
    {
        // If year is divisible by 100 but not 400, it is NOT a leap year.
        leap = 0; 
    }
    else if ((year % 4) == 0)
    {
        // Otherwise, if year is divisible by 4, it is a leap year.
        leap = 1;
    }
    return leap;
}


/**************************************
 * Dst::monthDays()
 **************************************/ 
int Dst::monthDays(int month)
{
    if ((month < 1) || (month > 12)) return 0;
    return LAST_DAY[month];
}


/**************************************
 * Dst::nthSunday()
 **************************************/ 
int Dst::nthSunday(int year, int month, int n)
{
    int day;
    
    dst_time_t time;
    time.year  = year;
    time.month = month;
    time.day   = 1;
    
    // Get the weekday of of the first day of month.
    // Use it to compute the date of the first Sunday of the month.
    int w1 = Dst::getWeekday(time); 
    if (w1 == 1)
    {
        day = 1;  // The first day of the Month falls on a Sunday.
    }
    else
    {
        day = 9 - w1;  // The first day of the Month falls on a Monday - Saturday.
    }
    
    // Compute the day of the Nth Sunday of the month for N = 1 - 4.
    if (n < 5)
    {
        day += 7 * (n-1);
    }
    else
    {
        // Compute the last Sunday of the month.
        day += 21;
        
        // Special case for February of a leap year.
        if ((month == 2) && Dst::isLeapYear(year))
        {
            if ((day + 7) <= 29)
            {
                // Fifth Sunday in February happens about once in 28 years.
                day += 7;
            }
        }
        else
        {
            if ((day + 7) <= LAST_DAY[month])
            {
                // Fifth Sunday of the month.
                day += 7;
            }
        }
    }
    
    return day;
}


/*****************************************************************************
 * Private methods and functions.
 ******************************************************************************/

 
// End of file.
