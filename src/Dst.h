/******************************************************************************
 * Dst.h
 * Copyright (c) 2017 Thomas Kerr AB3GY
 *
 * Released under the MIT License (MIT). 
 * See http://opensource.org/licenses/MIT
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 ******************************************************************************/

/**
 * @file
 * @brief
 * A self-contained class to manage Daylight Savings Time.
 *
 * Also includes methods for time and date calculations.
 *
 * Initially designed for Arduino platforms, although it can be used for other
 * embedded systems since it has no Arduino dependencies.
 *
 * The only system dependency is <time.h> for the definition of struct tm - the
 * standard structure containing a calendar date and time broken down into its 
 * components.  The class does not expect any time functions to be provided
 * by the system.  It is completely self-contained.
 */

#ifndef _DST_H
#define _DST_H

/******************************************************************************
 * System include files.
 ******************************************************************************/
#include <stdint.h>
#include <time.h>


/******************************************************************************
 * Local include files.
 ******************************************************************************/


/******************************************************************************
 * Public definitions.
 ******************************************************************************/
 
/**
 * @brief Epoch designators to use for time conversions.
 */
typedef enum
{
    DST_EPOCH_UNIX = 0,  //!< Unix epoch, 1970-01-01t00:00:00
    DST_EPOCH_NTP,       //!< NTP epoch,  1900-01-01t00:00:00
} dst_epoch_t;
 
 
/**
 * @brief Daylight Savings Time (DST) status.
 *
 * Values are consistent with the NIST WWVB timecode format.
 */
typedef enum
{
    DST_STANDARD_TIME = 0,
    DST_ENDING = 1,
    DST_BEGINNING = 2,
    DST_IN_EFFECT = 3,
} dst_status_t;


/**
 * @brief Structure to hold Daylight Savings Time (DST) parameters.
 */
typedef struct
{
    uint8_t enabled;         //!< 1 = DST is enabled
    uint8_t start_month;     //!< Starting month to "spring ahead" (1-12)
    uint8_t start_sunday;    //!< Sunday number to "spring ahead" (1-5, 5 = last Sunday of the month)
    uint8_t start_hour;      //!< Hour at which to turn DST on (usually 1 or 2)
    uint8_t end_month;       //!< Ending month to "fall back" (1-12)
    uint8_t end_sunday;      //!< Sunday number to "fall back" (1-5, 5 = last Sunday of the month)
    uint8_t end_hour;        //!< Hour at which to turn DST off (usually 1 or 2)
    int32_t offset_secs;     //!< Daylight savings time offset from standard time (usually +3600)
} dst_config_t;


/**
 * @brief Structure to hold time parameters relevant to DST.
 */ 
typedef struct
{
    int year;   //!< 4-digit absolute year (e.g., 1984, 2012)
    int month;  //!< Month (1-12)
    int day;    //!< Day of the month (1-31)
    int hour;   //!< Hour (0-59)
    int min;    //!< Minute (0-59)
    int sec;    //!< Second (0-59)
} dst_time_t;


/******************************************************************************
 * Public classes and functions.
 ******************************************************************************/

/**
 * @class
 * @brief A class to manage Daylight Savings Time.
 *
 * Also includes methods for time and date calculations.
 *
 * A Dst object is configured with a dst_config_t structure that describes the  
 * Daylight Savings Time rules for the local region of interest.  This allows 
 * the Dst  class to work in any region.  Once configured, the user can call 
 * Dst::isDst() or Dst::status() with a time structure to determine if Daylight 
 * Savings Time is in effect.
 */
class Dst
{
public:
    /**
     * @brief Default constructor.
     */
    Dst();
    
    /**
     * @brief Construct with the specified DST configuration.
     *
     * @param config DST configuration to use
     */
    Dst(const dst_config_t& config);
    
    /**
     * @brief Return the DST configuration structure.
     */
    const dst_config_t& getConfig() {return m_config;}
     
    /**
     * @brief Set the configuration to use for DST calculations.
     *
     * @param config DST configuration to use
     */
    void setConfig(const dst_config_t& config);
    
    /**
     * @brief Determine if DST is in effect for the specified time.
     *
     * Requires a DST configuration to be set prior to use.
     *
     * @param time The time structure to check
     *
     * @return 1 if DST is in effect, 0 otherwise
     */
    int isDst(const dst_time_t& time);
    
    /**
     * @brief Return the DST offset in seconds for the specified time.
     *
     * Returns either zero or the DST offset specified in the
     * configuration.
     *
     * @param time The time structure to check
     *
     * @return The DST offset in seconds
     */
    int32_t dstOffset(const dst_time_t& time);
    
    /**
     * @brief Return the DST status for the specified time.
     *
     * The status corresponds to the DST status contained
     * in the NIST WWVB timecode.
     *
     * Requires a DST configuration to be set prior to use.
     *
     * @param time The time structure to check
     *
     * @return The DST status
     */
    dst_status_t status(const dst_time_t& time);
    
    /**
     * @brief Convert a standard struct tm time to a dst_time_t type.
     *
     * Performs proper conversion of years and months.
     *
     * No range checking is performed on the input time structure.
     *
     * @param tmptr Pointer to the struct tm time to convert
     *
     * @param dstptr Pointer to the converted dst_time_t time
     */
    static void tm2dst(const struct tm* tmptr, dst_time_t* dstptr);
    
    /**
     * @brief Convert a dst_time_t type to a standard struct tm time.
     *
     * Performs proper conversion of years and months.  Also 
     * calculates tm_wday, tm_yday and tm_isdst fields.
     *
     * No range checking is performed on the input time structure.
     *
     * @param dstptr Pointer to the dst_time_t time to convert
     *
     * @param tmptr Pointer to the converted struct tm time
     */
    void dst2tm(const dst_time_t* dstptr, struct tm* tmptr);
    
    /**
     * @brief Return the day number (1-366) for the specified time
     *
     * @param time The time structure to check
     *
     * @return The corresponding day number (1-366)
     */
    static int getDayNumber(const dst_time_t& time);
    
    /**
     * @brief Compute the day of the week for the specified time.
     *
     * 1 = Sunday, 2 = Monday, ... 7 = Saturday
     *
     * @param time The time structure to check
     *
     * @return The corresponding day of the week (1-7)
     */
    static int getWeekday(const dst_time_t& time);
    
    /**
     * @brief Convert time_t to struct tm as UTC time
     *
     * Use the specified numeric time to fill a tm structure with the 
     * values  that represent the corresponding time, expressed as a 
     * UTC time (i.e., the time at the GMT timezone).
     *
     * Only works for times since the start of year 1972.
     *
     * @param time The time value to convert, in seconds since the referenced epoch
     *
     * @param tmptr Pointer to a tm structure to receive the converted time. 
     *
     * @param epoch The epoch reference for the time value, default = Unix (1970)
     */
    static void gmtime(uint32_t timer, struct tm* tmptr, dst_epoch_t epoch = DST_EPOCH_UNIX);
    
    /**
     * @brief Convert time_t to dst_time_t as UTC time
     *
     * Use the specified numeric time to fill a dst_time_t structure with the 
     * values  that represent the corresponding time, expressed as a 
     * UTC time (i.e., the time at the GMT timezone).
     *
     * Only works for times since the start of year 1972.
     *
     * @param time The time value to convert, in seconds since the referenced epoch
     *
     * @param dstptr Pointer to a dst_time_t structure to receive the converted time. 
     *
     * @param epoch The epoch reference for the time value, default = Unix (1970)
     */
    static void gmtime(uint32_t timer, dst_time_t* dstptr, dst_epoch_t epoch = DST_EPOCH_UNIX);
    
    /**
     * @brief Convert dst_time_t to time_t to as UTC time
     *
     * Returns a number that represents the time described by a dst_time_t structure.
     * The value is the number of seconds since the referenced epoch (Unix or NTP).
     * the dst_time_t structure is assumed to represent UTC time (i.e., the time at 
     * the GMT timezone).
     *
     * Only works for times since the start of year 1972.
     *
     * Only limited range checking is performed.  Some members of the dst_time_t structure 
     * may get interpreted even if out of range.  For example, February 31 may result in
     * a non-zero return value even though it is invalid.  The caller should ensure the 
     * integrity of the calendar elements.
     *
     * @param dstptr Pointer to a dst_time_t structure that contains a calendar time 
     * broken down into its components.
     *
     * @param epoch The epoch reference for the time value, default = Unix (1970)
     *
     * @return An integer value corresponding to the calendar time passed as an argument.
     * Returns zero if the calendar time cannot be converted.
     */
    static uint32_t mktime(const dst_time_t* dstptr, dst_epoch_t epoch = DST_EPOCH_UNIX);

    /**
     * @brief Check if specified year is a leap year.
     *
     * @param year The 4-digit absolute year to check (e.g., 1984, 2012)
     *
     * @return 1 if the 4-digit year is a leap year, 0 otherwise.
     */
    static int isLeapYear(int year);
    
    /**
     * @brief Return the number of days for the specified month in a non-leap year.
     *
     * @param month The month to check (1-12)
     *
     * @return The total number of days in the month for a non-leap year (1-31).
     * Returns zero if the month is invalid.
     */
    static int monthDays(int month);
    
    /**
     * @brief Return the day of the month for the Nth Sunday
     * of the month.
     *
     * @param year The 4-digit absolute year (e.g., 1984, 2012)
     *
     * @param month The month (1-12)
     *
     * @param n The Nth Sunday to calculate.  If n > 4, then the last
     * Sunday of the month is calculated.
     *
     * @return The day of the month for the Nth Sunday (1-31)
     */
    static int nthSunday(int year, int month, int n);
    
private:

    dst_config_t m_config;  //!< DST configuration for this object
};


#endif // _DST_H
