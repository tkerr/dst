/******************************************************************************
 * DstTest.ino
 * Copyright (c) 2019 Thomas Kerr AB3GY
 *
 * Released under the MIT License (MIT). 
 * See http://opensource.org/licenses/MIT
 *
 * THE SOFTWARE IS PROVIDED "AS IS", WITHOUT WARRANTY OF ANY KIND,
 * EXPRESS OR IMPLIED, INCLUDING BUT NOT LIMITED TO THE WARRANTIES OF
 * MERCHANTABILITY, FITNESS FOR A PARTICULAR PURPOSE AND NONINFRINGEMENT.
 * IN NO EVENT SHALL THE AUTHORS BE LIABLE FOR ANY CLAIM, DAMAGES OR
 * OTHER LIABILITY, WHETHER IN AN ACTION OF CONTRACT, TORT OR OTHERWISE,
 * ARISING FROM, OUT OF OR IN CONNECTION WITH THE SOFTWARE OR THE USE OR
 * OTHER DEALINGS IN THE SOFTWARE.
 ******************************************************************************/

/**
 * @file
 * @brief Unit test for the Dst class.
 *
 * Requires the aunit Arduino unit test library.
 */
 
/******************************************************************************
 * Lint options.
 ******************************************************************************/
 
 
/******************************************************************************
 * System include files.
 ******************************************************************************/
#include <Arduino.h>


/******************************************************************************
 * Local include files.
 ******************************************************************************/
#include "aunit.h"
#include "Dst.h"


/******************************************************************************
 * Forward references.
 ******************************************************************************/
static const char* dst2string(const dst_time_t* dstptr, char* str);
static const char* tm2string(const struct tm* tmptr, char* str);
static void leap_year_test();
static void day_number_test();
static void weekday_test();
static void nth_sunday_test();
static void dst_start_test();
static void dst_end_test();
static void gmtime_unix_test();
static void gmtime_ntp_test();


/******************************************************************************
 * Local definitions.
 ******************************************************************************/
#define SERIAL_BAUD  (115200)
#define UNIX2NTP     (2208988800UL)  //!< Conversion from Unix epoch to NTP epoch


/******************************************************************************
 * Global objects and data.
 ******************************************************************************/

 
/******************************************************************************
 * Local data.
 ******************************************************************************/

// US DST configuration.
static dst_config_t dstConfigUS = {1, 3, 2, 2, 11, 1, 2, 3600};

static dst_time_t testTime;
static struct tm  testTm;
static Dst myDst;
static uint16_t testNum = 0;
static char strBuf[32];


/******************************************************************************
 * Public functions.
 ******************************************************************************/
 
 
/**************************************
 * setup()
 **************************************/ 
void setup()
{
    // Initialize serial port and wait for it to connect.
    Serial.begin(SERIAL_BAUD);
    while (!Serial) {}
    Serial.println("DST Test");
    
    myDst.setConfig(dstConfigUS);
}


/**************************************
 * loop()
 **************************************/ 
void loop()
{
    // Wait for user to initiate test.
    TEST_INIT();
    TEST_FILE();
    TEST_WAIT();
    testNum = 0;
    
    TEST_NUMBER(++testNum);
    Serial.println("Leap Year Test");
    leap_year_test();
    
    TEST_NUMBER(++testNum);
    Serial.println("Day Number Test");
    day_number_test();
    
    TEST_NUMBER(++testNum);
    Serial.println("Weekday Test");
    weekday_test();
    
    TEST_NUMBER(++testNum);
    Serial.println("Nth Sunday Test");
    nth_sunday_test();
    
    TEST_NUMBER(++testNum);
    Serial.println("DST Start Test");
    dst_start_test();
    
    TEST_NUMBER(++testNum);
    Serial.println("DST End Test");
    dst_end_test();
    
    TEST_NUMBER(++testNum);
    Serial.println("gmtime/mktime Unix Test");
    gmtime_unix_test();
    
    TEST_NUMBER(++testNum);
    Serial.println("gmtime NTP Test");
    gmtime_ntp_test();
    
    // End of test.  Print stats.
    uint32_t asserts = TEST_ASSERT_COUNT();
    uint32_t passCnt = TEST_PASS_COUNT();
    uint32_t failCnt = asserts - passCnt;
    Serial.print("Total asserts: "); Serial.println(asserts);
    Serial.print("Total pass:    "); Serial.println(passCnt);
    Serial.print("Total fail:    "); Serial.println(failCnt);
    TEST_DONE();
}


/******************************************************************************
 * Private functions.
 ******************************************************************************/
 
/**************************************
 * dst2string()
 **************************************/  
static const char* dst2string(const dst_time_t* dstptr, char* str)
{
    sprintf(
        str, 
        "%04d-%02d-%02d %02d:%02d:%02d",
        dstptr->year,
        dstptr->month,
        dstptr->day,
        dstptr->hour,
        dstptr->min,
        dstptr->sec);
    return str;
}


/**************************************
 * tm2string()
 **************************************/ 
static const char* tm2string(const struct tm* tmptr, char* str)
{
    sprintf(
        str, 
        "%04d-%02d-%02d %02d:%02d:%02d %d %d %d",
        tmptr->tm_year + 1900,
        tmptr->tm_mon + 1,
        tmptr->tm_mday,
        tmptr->tm_hour,
        tmptr->tm_min,
        tmptr->tm_sec,
        tmptr->tm_yday + 1,
        tmptr->tm_wday,
        tmptr->tm_isdst);
    return str;
}


/**************************************
 * leap_year_test()
 **************************************/ 
static void leap_year_test()
{
    TEST_ASSERT_FAIL(Dst::isLeapYear(1900) == 0);
    TEST_ASSERT_FAIL(Dst::isLeapYear(1904) == 1);
    
    TEST_ASSERT_FAIL(Dst::isLeapYear(1999) == 0);
    TEST_ASSERT_FAIL(Dst::isLeapYear(2000) == 1);
    TEST_ASSERT_FAIL(Dst::isLeapYear(2001) == 0);
    TEST_ASSERT_FAIL(Dst::isLeapYear(2002) == 0);
    TEST_ASSERT_FAIL(Dst::isLeapYear(2003) == 0);
    TEST_ASSERT_FAIL(Dst::isLeapYear(2004) == 1);
    
    TEST_ASSERT_FAIL(Dst::isLeapYear(2019) == 0);
    TEST_ASSERT_FAIL(Dst::isLeapYear(2020) == 1);
    TEST_ASSERT_FAIL(Dst::isLeapYear(2021) == 0);
}


/**************************************
 * day_number_test()
 **************************************/ 
static void day_number_test()
{
    // Non-leap year.
    testTime.year  = 2019;
    testTime.month = 1;
    testTime.day   = 1;
    testTime.hour  = 0;
    testTime.min   = 0;
    testTime.sec   = 0;
    TEST_ASSERT_FAIL(Dst::getDayNumber(testTime) == 1);
    testTime.day   = 2;
    TEST_ASSERT_FAIL(Dst::getDayNumber(testTime) == 2);
    testTime.day   = 31;
    TEST_ASSERT_FAIL(Dst::getDayNumber(testTime) == 31);
    testTime.month = 2;
    testTime.day   = 1;
    TEST_ASSERT_FAIL(Dst::getDayNumber(testTime) == 32);
    testTime.day   = 28;
    TEST_ASSERT_FAIL(Dst::getDayNumber(testTime) == 59);
    testTime.month = 3;
    testTime.day   = 1;
    TEST_ASSERT_FAIL(Dst::getDayNumber(testTime) == 60);
    testTime.day   = 31;
    TEST_ASSERT_FAIL(Dst::getDayNumber(testTime) == 90);
    testTime.month = 4;
    testTime.day   = 1;
    TEST_ASSERT_FAIL(Dst::getDayNumber(testTime) == 91);
    testTime.month = 5;
    TEST_ASSERT_FAIL(Dst::getDayNumber(testTime) == 121);
    testTime.month = 6;
    TEST_ASSERT_FAIL(Dst::getDayNumber(testTime) == 152);
    testTime.month = 7;
    TEST_ASSERT_FAIL(Dst::getDayNumber(testTime) == 182);
    testTime.month = 8;
    TEST_ASSERT_FAIL(Dst::getDayNumber(testTime) == 213);
    testTime.month = 9;
    TEST_ASSERT_FAIL(Dst::getDayNumber(testTime) == 244);
    testTime.month = 10;
    TEST_ASSERT_FAIL(Dst::getDayNumber(testTime) == 274);
    testTime.month = 11;
    TEST_ASSERT_FAIL(Dst::getDayNumber(testTime) == 305);
    testTime.month = 12;
    TEST_ASSERT_FAIL(Dst::getDayNumber(testTime) == 335);
    testTime.day   = 31;
    TEST_ASSERT_FAIL(Dst::getDayNumber(testTime) == 365);
    
    // Leap year.
    testTime.year  = 2020;
    testTime.month = 1;
    testTime.day   = 1;
    TEST_ASSERT_FAIL(Dst::getDayNumber(testTime) == 1);
    testTime.day   = 2;
    TEST_ASSERT_FAIL(Dst::getDayNumber(testTime) == 2);
    testTime.day   = 31;
    TEST_ASSERT_FAIL(Dst::getDayNumber(testTime) == 31);
    testTime.month = 2;
    testTime.day   = 1;
    TEST_ASSERT_FAIL(Dst::getDayNumber(testTime) == 32);
    testTime.day   = 28;
    TEST_ASSERT_FAIL(Dst::getDayNumber(testTime) == 59);
    testTime.day   = 29;
    TEST_ASSERT_FAIL(Dst::getDayNumber(testTime) == 60);
    testTime.month = 3;
    testTime.day   = 1;
    TEST_ASSERT_FAIL(Dst::getDayNumber(testTime) == 61);
    testTime.day   = 31;
    TEST_ASSERT_FAIL(Dst::getDayNumber(testTime) == 91);
    testTime.month = 4;
    testTime.day   = 1;
    TEST_ASSERT_FAIL(Dst::getDayNumber(testTime) == 92);
    testTime.month = 5;
    TEST_ASSERT_FAIL(Dst::getDayNumber(testTime) == 122);
    testTime.month = 6;
    TEST_ASSERT_FAIL(Dst::getDayNumber(testTime) == 153);
    testTime.month = 7;
    TEST_ASSERT_FAIL(Dst::getDayNumber(testTime) == 183);
    testTime.month = 8;
    TEST_ASSERT_FAIL(Dst::getDayNumber(testTime) == 214);
    testTime.month = 9;
    TEST_ASSERT_FAIL(Dst::getDayNumber(testTime) == 245);
    testTime.month = 10;
    TEST_ASSERT_FAIL(Dst::getDayNumber(testTime) == 275);
    testTime.month = 11;
    TEST_ASSERT_FAIL(Dst::getDayNumber(testTime) == 306);
    testTime.month = 12;
    TEST_ASSERT_FAIL(Dst::getDayNumber(testTime) == 336);
    testTime.day   = 31;
    TEST_ASSERT_FAIL(Dst::getDayNumber(testTime) == 366);
}


/**************************************
 * weekday_test()
 **************************************/
static void weekday_test()
{
    // Non-leap year.
    testTime.year  = 2019;
    testTime.month = 10;
    int wday = 3;  // 2019-10-01 is a Tuesday
    for (int d = 1; d <= 31; d++)
    {
        testTime.day = d;
        TEST_ASSERT_FAIL(Dst::getWeekday(testTime) == wday);
        wday++;
        if (wday > 7) wday = 1;
    }
    
    // Leap year.
    testTime.year  = 2020;
    testTime.month = 2;
    wday = 7;  // 2020-02-01 is a Saturday
    for (int d = 1; d <= 29; d++)
    {
        testTime.day = d;
        TEST_ASSERT_FAIL(Dst::getWeekday(testTime) == wday);
        wday++;
        if (wday > 7) wday = 1;
    }
}


/**************************************
 * nth_sunday_test()
 **************************************/ 
static void nth_sunday_test()
{
    // 2019 February 3, 10, 17, 24 are Sundays.
    TEST_ASSERT_FAIL(Dst::nthSunday(2019, 2, 1) ==  3);
    TEST_ASSERT_FAIL(Dst::nthSunday(2019, 2, 2) == 10);
    TEST_ASSERT_FAIL(Dst::nthSunday(2019, 2, 3) == 17);
    TEST_ASSERT_FAIL(Dst::nthSunday(2019, 2, 4) == 24);
    TEST_ASSERT_FAIL(Dst::nthSunday(2019, 2, 5) == 24);
    
    // 2019 March 3, 10, 17, 24, 31 are Sundays.
    TEST_ASSERT_FAIL(Dst::nthSunday(2019, 3, 1) ==  3);
    TEST_ASSERT_FAIL(Dst::nthSunday(2019, 3, 2) == 10);
    TEST_ASSERT_FAIL(Dst::nthSunday(2019, 3, 3) == 17);
    TEST_ASSERT_FAIL(Dst::nthSunday(2019, 3, 4) == 24);
    TEST_ASSERT_FAIL(Dst::nthSunday(2019, 3, 5) == 31);
    
    // 2019 October 6, 13, 20, 27 are Sundays.
    TEST_ASSERT_FAIL(Dst::nthSunday(2019, 10, 1) ==  6);
    TEST_ASSERT_FAIL(Dst::nthSunday(2019, 10, 2) == 13);
    TEST_ASSERT_FAIL(Dst::nthSunday(2019, 10, 3) == 20);
    TEST_ASSERT_FAIL(Dst::nthSunday(2019, 10, 4) == 27);
    TEST_ASSERT_FAIL(Dst::nthSunday(2019, 10, 5) == 27);
    
    // 2019 December 1, 8, 15, 22, 29 are Sundays.
    TEST_ASSERT_FAIL(Dst::nthSunday(2019, 12, 1) ==  1);
    TEST_ASSERT_FAIL(Dst::nthSunday(2019, 12, 2) ==  8);
    TEST_ASSERT_FAIL(Dst::nthSunday(2019, 12, 3) == 15);
    TEST_ASSERT_FAIL(Dst::nthSunday(2019, 12, 4) == 22);
    TEST_ASSERT_FAIL(Dst::nthSunday(2019, 12, 5) == 29);
    
    // 2004 February 1, 8, 15, 22, 29 are Sundays.
    TEST_ASSERT_FAIL(Dst::nthSunday(2004, 2, 1) ==  1);
    TEST_ASSERT_FAIL(Dst::nthSunday(2004, 2, 2) ==  8);
    TEST_ASSERT_FAIL(Dst::nthSunday(2004, 2, 3) == 15);
    TEST_ASSERT_FAIL(Dst::nthSunday(2004, 2, 4) == 22);
    TEST_ASSERT_FAIL(Dst::nthSunday(2004, 2, 5) == 29);
}


/**************************************
 * dst_start_test()
 **************************************/ 
static void dst_start_test()
{
    // DST starts on 2019-03-10 02:00:00.
    testTime.year  = 2019;
    testTime.month = 3;
    testTime.day   = 9;
    testTime.hour  = 23;
    testTime.min   = 59;
    testTime.sec   = 59;
    TEST_ASSERT_FAIL(myDst.isDst(testTime) == 0);
    TEST_ASSERT_FAIL(myDst.status(testTime) == DST_STANDARD_TIME);
    TEST_ASSERT_FAIL(myDst.dstOffset(testTime) == 0);
    
    testTime.day  = 10;
    testTime.hour = 0;
    testTime.min  = 0;
    testTime.sec  = 0;
    TEST_ASSERT_FAIL(myDst.isDst(testTime) == 0);
    TEST_ASSERT_FAIL(myDst.status(testTime) == DST_BEGINNING);
    TEST_ASSERT_FAIL(myDst.dstOffset(testTime) == 0);
    
    testTime.hour = 1;
    testTime.min  = 59;
    testTime.sec  = 59;
    TEST_ASSERT_FAIL(myDst.isDst(testTime) == 0);
    TEST_ASSERT_FAIL(myDst.status(testTime) == DST_BEGINNING);
    TEST_ASSERT_FAIL(myDst.dstOffset(testTime) == 0);
    
    testTime.hour = 2;
    testTime.min  = 0;
    testTime.sec  = 0;
    TEST_ASSERT_FAIL(myDst.isDst(testTime) == 1);
    TEST_ASSERT_FAIL(myDst.status(testTime) == DST_IN_EFFECT);
    TEST_ASSERT_FAIL(myDst.dstOffset(testTime) == dstConfigUS.offset_secs);
}


/**************************************
 * dst_end_test()
 **************************************/ 
static void dst_end_test()
{
    // DST ends on 2019-11-03 02:00:00.
    testTime.year  = 2019;
    testTime.month = 11;
    testTime.day   = 2;
    testTime.hour  = 23;
    testTime.min   = 59;
    testTime.sec   = 59;
    TEST_ASSERT_FAIL(myDst.isDst(testTime) == 1);
    TEST_ASSERT_FAIL(myDst.status(testTime) == DST_IN_EFFECT);
    TEST_ASSERT_FAIL(myDst.dstOffset(testTime) == dstConfigUS.offset_secs);
    
    testTime.day  = 3;
    testTime.hour = 0;
    testTime.min  = 0;
    testTime.sec  = 0;
    TEST_ASSERT_FAIL(myDst.isDst(testTime) == 1);
    TEST_ASSERT_FAIL(myDst.status(testTime) == DST_ENDING);
    TEST_ASSERT_FAIL(myDst.dstOffset(testTime) == dstConfigUS.offset_secs);
    
    testTime.hour = 1;
    testTime.min  = 59;
    testTime.sec  = 59;
    TEST_ASSERT_FAIL(myDst.isDst(testTime) == 1);
    TEST_ASSERT_FAIL(myDst.status(testTime) == DST_ENDING);
    TEST_ASSERT_FAIL(myDst.dstOffset(testTime) == dstConfigUS.offset_secs);
    
    testTime.hour = 2;
    testTime.min  = 0;
    testTime.sec  = 0;
    TEST_ASSERT_FAIL(myDst.isDst(testTime) == 0);
    TEST_ASSERT_FAIL(myDst.status(testTime) == DST_STANDARD_TIME);
    TEST_ASSERT_FAIL(myDst.dstOffset(testTime) == 0);
}


/**************************************
 * gmtime_unix_test()
 **************************************/ 
static void gmtime_unix_test()
{
    // Test gmtime() and mktime() for two full leap year spans.
    // Time values obtained from https://www.epochconverter.com
    
    uint32_t t;
    
    // Non-leap year to leap year rollover.
    // 1999-12-31 23:59:59
    t = 946684799;
    Dst::gmtime(t, &testTm);
    Serial.println(tm2string(&testTm, strBuf));
    TEST_ASSERT_FAIL(strcmp(strBuf, "1999-12-31 23:59:59 365 5 0") == 0);
    Dst::gmtime(t, &testTime);
    dst2string(&testTime, strBuf);
    TEST_ASSERT_FAIL(strcmp(strBuf, "1999-12-31 23:59:59") == 0);
    TEST_ASSERT_FAIL(Dst::mktime(&testTime) == t);  // mktime() test
    
    
    // 2000-01-01 00:00:00
    t++;
    Dst::gmtime(t, &testTm);
    Serial.println(tm2string(&testTm, strBuf));
    TEST_ASSERT_FAIL(strcmp(strBuf, "2000-01-01 00:00:00 1 6 0") == 0);
    Dst::gmtime(t, &testTime);
    dst2string(&testTime, strBuf);
    TEST_ASSERT_FAIL(strcmp(strBuf, "2000-01-01 00:00:00") == 0);
    TEST_ASSERT_FAIL(Dst::mktime(&testTime) == t);  // mktime() test
    
    // Leap year 1 check.
    // 2000-02-28 23:59:59
    t = 951782399;
    Dst::gmtime(t, &testTm);
    Serial.println(tm2string(&testTm, strBuf));
    TEST_ASSERT_FAIL(strcmp(strBuf, "2000-02-28 23:59:59 59 1 0") == 0);
    Dst::gmtime(t, &testTime);
    dst2string(&testTime, strBuf);
    TEST_ASSERT_FAIL(strcmp(strBuf, "2000-02-28 23:59:59") == 0);
    TEST_ASSERT_FAIL(Dst::mktime(&testTime) == t);  // mktime() test
    
    // 2000-02-29 00:00:00
    t++;
    Dst::gmtime(t, &testTm);
    Serial.println(tm2string(&testTm, strBuf));
    TEST_ASSERT_FAIL(strcmp(strBuf, "2000-02-29 00:00:00 60 2 0") == 0);
    Dst::gmtime(t, &testTime);
    dst2string(&testTime, strBuf);
    TEST_ASSERT_FAIL(strcmp(strBuf, "2000-02-29 00:00:00") == 0);
    TEST_ASSERT_FAIL(Dst::mktime(&testTime) == t);  // mktime() test
    
    // Leap year to non-leap year rollover.
    // 2000-12-31 23:59:59
    t = 978307199;
    Dst::gmtime(t, &testTm);
    Serial.println(tm2string(&testTm, strBuf));
    TEST_ASSERT_FAIL(strcmp(strBuf, "2000-12-31 23:59:59 366 0 0") == 0);
    Dst::gmtime(t, &testTime);
    dst2string(&testTime, strBuf);
    TEST_ASSERT_FAIL(strcmp(strBuf, "2000-12-31 23:59:59") == 0);
    TEST_ASSERT_FAIL(Dst::mktime(&testTime) == t);  // mktime() test
    
    // 2001-01-01 00:00:00
    t++;
    Dst::gmtime(t, &testTm);
    Serial.println(tm2string(&testTm, strBuf));
    TEST_ASSERT_FAIL(strcmp(strBuf, "2001-01-01 00:00:00 1 1 0") == 0);
    Dst::gmtime(t, &testTime);
    dst2string(&testTime, strBuf);
    TEST_ASSERT_FAIL(strcmp(strBuf, "2001-01-01 00:00:00") == 0);
    TEST_ASSERT_FAIL(Dst::mktime(&testTime) == t);  // mktime() test
    
    // Non-leap year 1 check.
    // 2001-02-28 23:59:59
    t = 983404799;
    Dst::gmtime(t, &testTm);
    Serial.println(tm2string(&testTm, strBuf));
    TEST_ASSERT_FAIL(strcmp(strBuf, "2001-02-28 23:59:59 59 3 0") == 0);
    Dst::gmtime(t, &testTime);
    dst2string(&testTime, strBuf);
    TEST_ASSERT_FAIL(strcmp(strBuf, "2001-02-28 23:59:59") == 0);
    TEST_ASSERT_FAIL(Dst::mktime(&testTime) == t);  // mktime() test
    
    // 2001-03-01 00:00:00
    t++;
    Dst::gmtime(t, &testTm);
    Serial.println(tm2string(&testTm, strBuf));
    TEST_ASSERT_FAIL(strcmp(strBuf, "2001-03-01 00:00:00 60 4 0") == 0);
    Dst::gmtime(t, &testTime);
    dst2string(&testTime, strBuf);
    TEST_ASSERT_FAIL(strcmp(strBuf, "2001-03-01 00:00:00") == 0);
    TEST_ASSERT_FAIL(Dst::mktime(&testTime) == t);  // mktime() test
    
    // Non-leap year 2 check.
    // 2002-02-28 23:59:59
    t = 1014940799;
    Dst::gmtime(t, &testTm);
    Serial.println(tm2string(&testTm, strBuf));
    TEST_ASSERT_FAIL(strcmp(strBuf, "2002-02-28 23:59:59 59 4 0") == 0);
    Dst::gmtime(t, &testTime);
    dst2string(&testTime, strBuf);
    TEST_ASSERT_FAIL(strcmp(strBuf, "2002-02-28 23:59:59") == 0);
    TEST_ASSERT_FAIL(Dst::mktime(&testTime) == t);  // mktime() test
    
    // 2002-03-01 00:00:00
    t++;
    Dst::gmtime(t, &testTm);
    Serial.println(tm2string(&testTm, strBuf));
    TEST_ASSERT_FAIL(strcmp(strBuf, "2002-03-01 00:00:00 60 5 0") == 0);
    Dst::gmtime(t, &testTime);
    dst2string(&testTime, strBuf);
    TEST_ASSERT_FAIL(strcmp(strBuf, "2002-03-01 00:00:00") == 0);
    TEST_ASSERT_FAIL(Dst::mktime(&testTime) == t);  // mktime() test
    
    // Non-leap year 3 check.
    // 2003-02-28 23:59:59
    t = 1046476799;
    Dst::gmtime(t, &testTm);
    Serial.println(tm2string(&testTm, strBuf));
    TEST_ASSERT_FAIL(strcmp(strBuf, "2003-02-28 23:59:59 59 5 0") == 0);
    Dst::gmtime(t, &testTime);
    dst2string(&testTime, strBuf);
    TEST_ASSERT_FAIL(strcmp(strBuf, "2003-02-28 23:59:59") == 0);
    TEST_ASSERT_FAIL(Dst::mktime(&testTime) == t);  // mktime() test
    
    // 2003-03-01 00:00:00
    t++;
    Dst::gmtime(t, &testTm);
    Serial.println(tm2string(&testTm, strBuf));
    TEST_ASSERT_FAIL(strcmp(strBuf, "2003-03-01 00:00:00 60 6 0") == 0);
    Dst::gmtime(t, &testTime);
    dst2string(&testTime, strBuf);
    TEST_ASSERT_FAIL(strcmp(strBuf, "2003-03-01 00:00:00") == 0);
    TEST_ASSERT_FAIL(Dst::mktime(&testTime) == t);  // mktime() test
    
    // Leap year 2 check.
    // 2004-02-28 23:59:59
    t = 1078012799;
    Dst::gmtime(t, &testTm);
    Serial.println(tm2string(&testTm, strBuf));
    TEST_ASSERT_FAIL(strcmp(strBuf, "2004-02-28 23:59:59 59 6 0") == 0);
    Dst::gmtime(t, &testTime);
    dst2string(&testTime, strBuf);
    TEST_ASSERT_FAIL(strcmp(strBuf, "2004-02-28 23:59:59") == 0);
    TEST_ASSERT_FAIL(Dst::mktime(&testTime) == t);  // mktime() test
    
    // 2004-02-29 00:00:00
    t++;
    Dst::gmtime(t, &testTm);
    Serial.println(tm2string(&testTm, strBuf));
    TEST_ASSERT_FAIL(strcmp(strBuf, "2004-02-29 00:00:00 60 0 0") == 0);
    Dst::gmtime(t, &testTime);
    dst2string(&testTime, strBuf);
    TEST_ASSERT_FAIL(strcmp(strBuf, "2004-02-29 00:00:00") == 0);
    TEST_ASSERT_FAIL(Dst::mktime(&testTime) == t);  // mktime() test
    
    // 2004-03-01 02:03:04
    t += 86400 + 7200 + 180 + 4;  // Add 1 day + 2 hours + 3 minutes + 4 seconds
    Dst::gmtime(t, &testTm);
    Serial.println(tm2string(&testTm, strBuf));
    TEST_ASSERT_FAIL(strcmp(strBuf, "2004-03-01 02:03:04 61 1 0") == 0); 
    Dst::gmtime(t, &testTime);
    dst2string(&testTime, strBuf);
    TEST_ASSERT_FAIL(strcmp(strBuf, "2004-03-01 02:03:04") == 0);
    TEST_ASSERT_FAIL(Dst::mktime(&testTime) == t);  // mktime() test
}


/**************************************
 * gmtime_ntp_test()
 **************************************/ 
static void gmtime_ntp_test()
{
    // Same as gmtime_unix_test() except with NTP epoch of 1900.
    
    uint32_t t;
    
    // Non-leap year to leap year rollover.
    // 1999-12-31 23:59:59
    t = 946684799 + UNIX2NTP;
    Dst::gmtime(t, &testTm, DST_EPOCH_NTP);
    Serial.println(tm2string(&testTm, strBuf));
    TEST_ASSERT_FAIL(strcmp(strBuf, "1999-12-31 23:59:59 365 5 0") == 0);
    
    // 2000-01-01 00:00:00
    t++;
    Dst::gmtime(t, &testTm, DST_EPOCH_NTP);
    Serial.println(tm2string(&testTm, strBuf));
    TEST_ASSERT_FAIL(strcmp(strBuf, "2000-01-01 00:00:00 1 6 0") == 0);
    
    // Leap year 1 check.
    // 2000-02-28 23:59:59
    t = 951782399 + UNIX2NTP;
    Dst::gmtime(t, &testTm, DST_EPOCH_NTP);
    Serial.println(tm2string(&testTm, strBuf));
    TEST_ASSERT_FAIL(strcmp(strBuf, "2000-02-28 23:59:59 59 1 0") == 0);
    
    // 2000-02-29 00:00:00
    t++;
    Dst::gmtime(t, &testTm, DST_EPOCH_NTP);
    Serial.println(tm2string(&testTm, strBuf));
    TEST_ASSERT_FAIL(strcmp(strBuf, "2000-02-29 00:00:00 60 2 0") == 0);
    
    // Leap year to non-leap year rollover.
    // 2000-12-31 23:59:59
    t = 978307199 + UNIX2NTP;
    Dst::gmtime(t, &testTm, DST_EPOCH_NTP);
    Serial.println(tm2string(&testTm, strBuf));
    TEST_ASSERT_FAIL(strcmp(strBuf, "2000-12-31 23:59:59 366 0 0") == 0);
    
    // 2001-01-01 00:00:00
    t++;
    Dst::gmtime(t, &testTm, DST_EPOCH_NTP);
    Serial.println(tm2string(&testTm, strBuf));
    TEST_ASSERT_FAIL(strcmp(strBuf, "2001-01-01 00:00:00 1 1 0") == 0);
    
    // Non-leap year 1 check.
    // 2001-02-28 23:59:59
    t = 983404799 + UNIX2NTP;
    Dst::gmtime(t, &testTm, DST_EPOCH_NTP);
    Serial.println(tm2string(&testTm, strBuf));
    TEST_ASSERT_FAIL(strcmp(strBuf, "2001-02-28 23:59:59 59 3 0") == 0);
    
    // 2001-03-01 00:00:00
    t++;
    Dst::gmtime(t, &testTm, DST_EPOCH_NTP);
    Serial.println(tm2string(&testTm, strBuf));
    TEST_ASSERT_FAIL(strcmp(strBuf, "2001-03-01 00:00:00 60 4 0") == 0);
    
    // Non-leap year 2 check.
    // 2002-02-28 23:59:59
    t = 1014940799 + UNIX2NTP;
    Dst::gmtime(t, &testTm, DST_EPOCH_NTP);
    Serial.println(tm2string(&testTm, strBuf));
    TEST_ASSERT_FAIL(strcmp(strBuf, "2002-02-28 23:59:59 59 4 0") == 0);
    
    // 2002-03-01 00:00:00
    t++;
    Dst::gmtime(t, &testTm, DST_EPOCH_NTP);
    Serial.println(tm2string(&testTm, strBuf));
    TEST_ASSERT_FAIL(strcmp(strBuf, "2002-03-01 00:00:00 60 5 0") == 0);
    
    // Non-leap year 3 check.
    // 2003-02-28 23:59:59
    t = 1046476799 + UNIX2NTP;
    Dst::gmtime(t, &testTm, DST_EPOCH_NTP);
    Serial.println(tm2string(&testTm, strBuf));
    TEST_ASSERT_FAIL(strcmp(strBuf, "2003-02-28 23:59:59 59 5 0") == 0);
    
    // 2003-03-01 00:00:00
    t++;
    Dst::gmtime(t, &testTm, DST_EPOCH_NTP);
    Serial.println(tm2string(&testTm, strBuf));
    TEST_ASSERT_FAIL(strcmp(strBuf, "2003-03-01 00:00:00 60 6 0") == 0);
    
    // Leap year 2 check.
    // 2004-02-28 23:59:59
    t = 1078012799 + UNIX2NTP;
    Dst::gmtime(t, &testTm, DST_EPOCH_NTP);
    Serial.println(tm2string(&testTm, strBuf));
    TEST_ASSERT_FAIL(strcmp(strBuf, "2004-02-28 23:59:59 59 6 0") == 0);
    
    // 2004-02-29 00:00:00
    t++;
    Dst::gmtime(t, &testTm, DST_EPOCH_NTP);
    Serial.println(tm2string(&testTm, strBuf));
    TEST_ASSERT_FAIL(strcmp(strBuf, "2004-02-29 00:00:00 60 0 0") == 0);
    
    // 2004-03-01 02:03:04
    t += 86400 + 7200 + 180 + 4;  // Add 1 day + 2 hours + 3 minutes + 4 seconds
    Dst::gmtime(t, &testTm, DST_EPOCH_NTP);
    Serial.println(tm2string(&testTm, strBuf));
    TEST_ASSERT_FAIL(strcmp(strBuf, "2004-03-01 02:03:04 61 1 0") == 0); 
}

// End of file.